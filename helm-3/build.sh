#!/bin/sh

BASE_DIR="$( cd $(dirname "$0") ; pwd)"
cd "${BASE_DIR}"

set -eux

# Build and push runner image
DOCKER_IMAGE="registry.gitlab.com/certida-public/containers/deployment/helm-3:k8s-$1"
docker pull "docker:stable"
docker build -t "${DOCKER_IMAGE}" --build-arg "KUBE_VERSION=v$1" .
docker push "${DOCKER_IMAGE}"
